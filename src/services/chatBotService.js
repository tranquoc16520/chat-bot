import request from "request";

require("dotenv").config();

const PAGE_ACCESS_TOKEN = process.env.PAGE_ACCESS_TOKEN;
const URL_SHOW_ROOM_GIF =
  "https://media3.giphy.com/media/TGcD6N8uzJ9FXuDV3a/giphy.gif?cid=ecf05e47afe5be971d1fe6c017ada8e15c29a76fc524ac20&rid=giphy.gif";
const URL_SALAD_GIF =
  "https://media0.giphy.com/media/9Vk8qP9EmWB8FePccb/giphy.gif?cid=ecf05e478d0c93d69e72264c8ebbf58a9a1d7ae294754131&rid=giphy.gif";
const URL_SHOW_FISH =
  "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/ztjeouq2jlas5b2zxksm";
const URL_SHOW_CLASSIC =
  "https://ardo.com/files/attachments/.10202/w1440h700q85_AZ1.jpg";

let getFacebookUsername = (sender_psid) => {
  return new Promise((resolve, reject) => {
    // Send the HTTP request to the Messenger Platform
    let uri = `https://graph.facebook.com/${sender_psid}?fields=first_name,last_name,profile_pic&access_token=${PAGE_ACCESS_TOKEN}`;
    request(
      {
        uri: uri,
        method: "GET",
      },
      (err, res, body) => {
        if (!err) {
          //convert string to json object
          body = JSON.parse(body);
          let username = `${body.last_name} ${body.first_name}`;
          resolve(username);
        } else {
          reject("Unable to send message:" + err);
        }
      }
    );
  });
};

let sendResponseWelcomeNewCustomer = (username, sender_psid) => {
  return new Promise(async (resolve, reject) => {
    try {
      let response_first = {
        text: `Xin chào ${username}! Cám ơn bạn đã quan tâm đến dịch vụ BookingCare!`,
      };
      let response_second = {
        attachment: {
          type: "template",
          payload: {
            template_type: "generic",
            elements: [
              {
                title: "BookingCare",
                subtitle: "Nền tảng chăm sóc sức khỏe toàn diện",
                image_url: "https://digital.fpt.com.vn/wp-content/uploads/2020/07/chamsoctuxa.jpg",
                buttons: [
                  {
                    type: "postback",
                    title: "DỊCH VỤ CHÍNH",
                    payload: "MAIN_MENU",
                  },
                  {
                    type: "postback",
                    title: "ĐẶT LỊCH KHÁM",
                    payload: "RESERVE_TABLE",
                  },
                  {
                    type: "postback",
                    title: "GUIDE TO USE THIS BOT",
                    payload: "GUIDE_BOT",
                  },
                ],
              },
            ],
          },
        },
      };

      //send a welcome message
      await sendTypingOn(sender_psid);
      await sendMessage(sender_psid, response_first);

      //send a image with button view main menu
      await sendTypingOn(sender_psid);
      await sendMessage(sender_psid, response_second);

      resolve("done!");
    } catch (e) {
      reject(e);
    }
  });
};

let sendMainMenu = (sender_psid) => {
  return new Promise(async (resolve, reject) => {
    try {
      let response = {
        attachment: {
          type: "template",
          payload: {
            template_type: "generic",
            elements: [
              {
                title: "DỊCH VỤ CỦA CHÚNG TÔI",
                subtitle:
                  "Vui lòng chọn gói dịch vụ phù hợp với nhu cầu của bạn.",
                image_url: "https://lh6.googleusercontent.com/wM2HrcZYmsQMOzhH5At6y7YTxj-cR6o5xkQCaB6isUyQ48JEBouDzXSoMafAEA5P-b4tGAgvxHFDe68e6sFK9-5ec9wa48vY-e5Ymbi-MHQov0vEN95C9L5Wo_Ozv167tmItcTntUjwAlY_CxQ",
                buttons: [
                  {
                    type: "postback",
                    title: "CHUYÊN KHOA",
                    payload: "LUNCH_MENU",
                  },
                  {
                    type: "postback",
                    title: "CƠ SỞ Y TẾ",
                    payload: "DINNER_MENU",
                  },
                  {
                    type: "postback",
                    title: "BÁC SĨ",
                    payload: "PUB_MENU",
                  },
                ],
              },

              {
                title: "ĐẶT LỊCH",
                subtitle: "T2-T7 9AM - 16PM",
                image_url: " https://tphcm.cdnchinhphu.vn/334895287454388224/2022/6/18/20220321010907798879hau-covid-19max-1800x1800-16555386852871404136005.jpg",
                buttons: [
                  {
                    type: "postback",
                    title: "ĐẶT LỊCH KHÁM",
                    payload: "RESERVE_TABLE",
                  },
                ],
              },

              {
                title: "CƠ SỞ Y TẾ ",
                subtitle: "Tìm kiếm bệnh viên, phòng khám",
                image_url: " https://cdn.bookingcare.vn/fr/w1200/2018/03/01/144158120555bv-bach-mai.jpg",
                buttons: [
                  {
                    type: "postback",
                    title: "CƠ SỞ Y TẾ",
                    payload: "SHOW_ROOMS",
                  },
                ],
              },
            ],
          },
        },
      };
      await sendTypingOn(sender_psid);
      await sendMessage(sender_psid, response);
      resolve("done");
    } catch (e) {
      reject(e);
    }
  });
};

let sendLunchMenu = (sender_psid) => {
  return new Promise(async (resolve, reject) => {
    try {
      let response = {
        attachment: {
          type: "template",
          payload: {
            template_type: "generic",
            elements: [
              {
                title: "CƠ XƯƠNG KHỚP",
                image_url:
                  "https://cdn.bookingcare.vn/fr/w300/2019/12/13/120331-co-xuong-khop.jpg",
                buttons: [
                  {
                    type: "postback",
                    title: "DANH SÁCH CÁC BÁC SĨ",
                    payload: "SHOW_APPETIZERS",
                  },
                ],
              },

              {
                title: "THẦN KINH",
                image_url:
                  "https://cdn.bookingcare.vn/fr/w300/2019/12/13/121042-than-kinh.jpg",
                buttons: [
                  {
                    type: "postback",
                    title: "DANH SÁCH CÁC BÁC SĨ",
                    payload: "SHOW_ENTREE_SALAD",
                  },
                ],
              },

              {
                title: "TIÊU HÓA",
                image_url:
                  "https://cdn.bookingcare.vn/fr/w300/2019/12/13/120933-tieu-hoa.jpg",
                buttons: [
                  {
                    type: "postback",
                    title: "DANH SÁCH CÁC BÁC SĨ",
                    payload: "SHOW_FISH",
                  },
                ],
              },

              {
                title: "TIM MẠCH",
                image_url:
                  "https://cdn.bookingcare.vn/fr/w300/2019/12/13/120741-tim-mach.jpg",
                buttons: [
                  {
                    type: "postback",
                    title: "DANH SÁCH CÁC BÁC SĨ",
                    payload: "SHOW_CLASSICS",
                  },
                ],
              },

              {
                title: "TAI MŨI HỌNG",
                image_url:
                  "https://cdn.bookingcare.vn/fr/w300/2019/12/13/121146-tai-mui-hong.jpg",
                buttons: [
                  {
                    type: "postback",
                    title: "DANH SÁCH CÁC BÁC SĨ",
                    payload: "SHOW_TMH",
                  },
                ],
              },

              {
                title: "Trở về",
                image_url: " https://digital.fpt.com.vn/wp-content/uploads/2020/07/chamsoctuxa.jpg",
                buttons: [
                  {
                    type: "postback",
                    title: "CÁC DỊCH VỤ",
                    payload: "BACK_TO_MAIN_MENU",
                  },
                  {
                    type: "postback",
                    title: "ĐẶT LỊCH KHÁM",
                    payload: "RESERVE_TABLE",
                  },
                ],
              },
            ],
          },
        },
      };
      await sendTypingOn(sender_psid);
      await sendMessage(sender_psid, response);
      resolve("done");
    } catch (e) {
      reject(e);
    }
  });
};

let sendDinnerMenu = (sender_psid) => {
  return new Promise(async (resolve, reject) => {
    try {
      let response1 = {
        text: "Lump crab cocktail\n$25.00",
      };
      let response2 = {
        attachment: {
          type: "image",
          payload: {
            url: "https://djfoodie.com/wp-content/uploads/Crab-Cocktail-3-800.jpg",
          },
        },
      };

      let response3 = {
        text: "House cured salmon\n$16.00",
      };
      let response4 = {
        attachment: {
          type: "image",
          payload: {
            url: "https://www.thespruceeats.com/thmb/rys3IyH2DB6Ma_r4IQ6emN-2jYw=/4494x3000/filters:fill(auto,1)/simple-homemade-gravlax-recipe-2216618_hero-01-592dadcba64743f98aa1f7a14f81d5b4.jpg",
          },
        },
      };

      let response5 = {
        text: "Steamed Whole Maine Lobsters\n$35.00",
      };
      let response6 = {
        attachment: {
          type: "image",
          payload: {
            url: "https://portcitydaily.com/wp-content/uploads/For-the-Shell-of-It.jpg",
          },
        },
      };

      let response7 = {
        attachment: {
          type: "template",
          payload: {
            template_type: "button",
            text: `CÁC DỊCH VỤ`,
            buttons: [
              {
                type: "postback",
                title: "DỊCH VỤ CHÍNH",
                payload: "MAIN_MENU",
              },
              {
                type: "postback",
                title: "ĐẶT LỊCH KHÁM",
                payload: "RESERVE_TABLE",
              },
            ],
          },
        },
      };

      await sendTypingOn(sender_psid);
      await sendMessage(sender_psid, response1);

      await sendTypingOn(sender_psid);
      await sendMessage(sender_psid, response2);

      await sendTypingOn(sender_psid);
      await sendMessage(sender_psid, response3);

      await sendTypingOn(sender_psid);
      await sendMessage(sender_psid, response4);

      await sendTypingOn(sender_psid);
      await sendMessage(sender_psid, response5);

      await sendTypingOn(sender_psid);
      await sendMessage(sender_psid, response6);

      await sendTypingOn(sender_psid);
      await sendMessage(sender_psid, response7);

      resolve("done");
    } catch (e) {
      reject(e);
    }
  });
};

let sendPubMenu = (sender_psid) => {
  return new Promise(async (resolve, reject) => {
    try {
      let response1 = {
        text: "Hamburger with French Fries\n$19.50",
      };
      let response2 = {
        attachment: {
          type: "image",
          payload: {
            url: "https://previews.123rf.com/images/genmike/genmike1411/genmike141100010/33951440-burger-and-french-fries.jpg",
          },
        },
      };

      let response3 = {
        text: "Ham and Cheese on a Baguette as Salad or Sandwich\n$21.00",
      };
      let response4 = {
        attachment: {
          type: "image",
          payload: {
            url: "https://s3-ap-southeast-1.amazonaws.com/v3-live.image.oddle.me/product/Blackforesthamcheesebfd18d.jpg",
          },
        },
      };

      let response5 = {
        text: "Braised short rib salad\n$29.50",
      };
      let response6 = {
        attachment: {
          type: "image",
          payload: {
            url: "https://www.bbcgoodfood.com/sites/default/files/styles/recipe/public/recipe_images/ribs_0.jpg?itok=bOf0t_NF",
          },
        },
      };

      let response7 = {
        attachment: {
          type: "template",
          payload: {
            template_type: "button",
            text: `CÁC DỊCH VỤ or make a reservation ?`,
            buttons: [
              {
                type: "postback",
                title: "DỊCH VỤ CHÍNH",
                payload: "MAIN_MENU",
              },
              {
                type: "postback",
                title: "ĐẶT LỊCH KHÁM",
                payload: "RESERVE_TABLE",
              },
            ],
          },
        },
      };

      await sendTypingOn(sender_psid);
      await sendMessage(sender_psid, response1);

      await sendTypingOn(sender_psid);
      await sendMessage(sender_psid, response2);

      await sendTypingOn(sender_psid);
      await sendMessage(sender_psid, response3);

      await sendTypingOn(sender_psid);
      await sendMessage(sender_psid, response4);

      await sendTypingOn(sender_psid);
      await sendMessage(sender_psid, response5);

      await sendTypingOn(sender_psid);
      await sendMessage(sender_psid, response6);

      await sendTypingOn(sender_psid);
      await sendMessage(sender_psid, response7);
      resolve("done");
    } catch (e) {
      reject(e);
    }
  });
};

let sendAppetizer = (sender_psid) => {
  return new Promise(async (resolve, reject) => {
    try {
      let response = {
        attachment: {
          type: "template",
          payload: {
            template_type: "generic",
            elements: [
              {
                title: "GS-TS Bùi Ngọc Ân",
                subtitle: `Nguyên Trưởng khoa Cơ xương khớp, Bệnh viện Bạch Mai
                                Chủ tịch Hội Thấp khớp học Việt Nam
                                Giáo sư đầu ngành với gần 50 năm kinh nghiệm điều trị các bệnh lý liên quan đến Cơ xương khớp
                                Bác sĩ khám cho người bệnh từ 14 tuổi trở lên
                                Giá khám: 500.000VNĐ`,
                image_url:
                  "https://cdn.bookingcare.vn/fr/w200/2018/04/09/151800292142135730131997187173031663525568184320n.jpg",
              },

              {
                title: "PGS-TS Vũ Thị Thanh Thủy",
                subtitle: `Nguyên Trưởng khoa Cơ - Xương - Khớp, Bệnh viện Bạch Mai
                                Chủ tịch Hội loãng xương Hà Nội
                                Bác sĩ nhận bệnh nhân từ 18 tuổi trở lên
                                Giá khám: 400.000VNĐ`,
                image_url:
                  "https://cdn.bookingcare.vn/fr/w200/2019/12/31/161245-pgs-vu-thi-thanh-thuy.jpg",
              },

              {
                title: "PGS-TS Ngô Văn Toàn",
                subtitle: `Nguyên Trưởng khoa Phẫu thuật chấn thương chỉnh hình - Bệnh viện Việt Đức
                                Nguyên Viện phó Viện Chấn thương chỉnh hình - Bệnh viện Việt Đức
                                Chuyên gia đầu ngành về chấn thương chỉnh hình tại Việt Nam
                                Được nhận Danh hiệu Thầy thuốc nhân dân
                                Giá khám: 500.000VNĐ`,
                image_url:
                  "https://cdn.bookingcare.vn/fr/w200/2018/02/11/165555bac-si-nguyen-van-toan.jpg",
              },

              {
                title: "Trở về",
                image_url: " https://digital.fpt.com.vn/wp-content/uploads/2020/07/chamsoctuxa.jpg",
                buttons: [
                  {
                    type: "postback",
                    title: "CHUYÊN KHOA",
                    payload: "BACK_TO_LUNCH_MENU",
                  },
                  {
                    type: "postback",
                    title: "CÁC DỊCH VỤ",
                    payload: "BACK_TO_MAIN_MENU",
                  },
                  {
                    type: "postback",
                    title: "ĐẶT LỊCH KHÁM",
                    payload: "RESERVE_TABLE",
                  },
                ],
              },
            ],
          },
        },
      };

      await sendTypingOn(sender_psid);
      await sendMessage(sender_psid, response);
    } catch (e) {
      reject(e);
    }
  });
};

let goBackToMainMenu = (sender_psid) => {
  sendMainMenu(sender_psid);
};

let goBackToLunchMenu = (sender_psid) => {
  sendLunchMenu(sender_psid);
};

let handleReserveTable = (sender_psid) => {
  return new Promise(async (resolve, reject) => {
    try {
      let username = await getFacebookUsername(sender_psid);
      let response = {
        text: `Hi ${username}, What time and date you would like to ĐẶT LỊCH KHÁM ?`,
      };
      await sendTypingOn(sender_psid);
      await sendMessage(sender_psid, response);
    } catch (e) {
      reject(e);
    }
  });
};

let handleShowRooms = (sender_psid) => {
  return new Promise(async (resolve, reject) => {
    try {
      let response = {
        attachment: {
          type: "template",
          payload: {
            template_type: "generic",
            elements: [
              {
                title: "Bull Moose Room",
                subtitle: "The room is suited for parties of up to 25 people",
                image_url: "https://bit.ly/showRoom1",
                buttons: [
                  {
                    type: "postback",
                    title: "SHOW DESCRIPTION",
                    payload: "SHOW_ROOM_DETAIL",
                  },
                ],
              },

              {
                title: "Lillie Langstry Room",
                subtitle: "The room is suited for parties of up to 35 people",
                image_url: "https://bit.ly/showRoom2",
                buttons: [
                  {
                    type: "postback",
                    title: "SHOW DESCRIPTION",
                    payload: "SHOW_ROOM_DETAIL",
                  },
                ],
              },

              {
                title: "Lincoln Room",
                subtitle: "The room is suited for parties of up to 45 people",
                image_url: "https://bit.ly/showRoom3",
                buttons: [
                  {
                    type: "postback",
                    title: "SHOW DESCRIPTION",
                    payload: "SHOW_ROOM_DETAIL",
                  },
                ],
              },

              {
                title: "Trở về",
                image_url: " https://digital.fpt.com.vn/wp-content/uploads/2020/07/chamsoctuxa.jpg",
                buttons: [
                  {
                    type: "postback",
                    title: "CÁC DỊCH VỤ",
                    payload: "BACK_TO_MAIN_MENU",
                  },
                  {
                    type: "postback",
                    title: "ĐẶT LỊCH KHÁM",
                    payload: "RESERVE_TABLE",
                  },
                ],
              },
            ],
          },
        },
      };

      //send a welcome message
      await sendTypingOn(sender_psid);
      await sendMessage(sender_psid, response);
    } catch (e) {
      reject(e);
    }
  });
};

let sendMessageAskingQuality = (sender_id) => {
  let request_body = {
    recipient: {
      id: sender_id,
    },
    messaging_type: "RESPONSE",
    message: {
      text: "What is your party size ?",
      quick_replies: [
        {
          content_type: "text",
          title: "1-2",
          payload: "SMALL",
        },
        {
          content_type: "text",
          title: "2-5",
          payload: "MEDIUM",
        },
        {
          content_type: "text",
          title: "more than 5",
          payload: "LARGE",
        },
      ],
    },
  };

  // Send the HTTP request to the Messenger Platform
  request(
    {
      uri: "https://graph.facebook.com/v6.0/me/messages",
      qs: { access_token: PAGE_ACCESS_TOKEN },
      method: "POST",
      json: request_body,
    },
    (err, res, body) => {
      if (!err) {
        console.log("message sent!");
      } else {
        console.error("Unable to send message:" + err);
      }
    }
  );
};

let sendMessageAskingPhoneNumber = (sender_id) => {
  let request_body = {
    recipient: {
      id: sender_id,
    },
    messaging_type: "RESPONSE",
    message: {
      text: "Thank you. And what's the best phone number for us to reach you at?",
      quick_replies: [
        {
          content_type: "user_phone_number",
        },
      ],
    },
  };

  // Send the HTTP request to the Messenger Platform
  request(
    {
      uri: "https://graph.facebook.com/v6.0/me/messages",
      qs: { access_token: PAGE_ACCESS_TOKEN },
      method: "POST",
      json: request_body,
    },
    (err, res, body) => {
      if (!err) {
        console.log("message sent!");
      } else {
        console.error("Unable to send message:" + err);
      }
    }
  );
};

let sendMessageDoneReserveTable = async (sender_id) => {
  try {
    let response = {
      attachment: {
        type: "image",
        payload: {
          url: "https://bit.ly/giftDonalTrump",
        },
      },
    };
    await sendTypingOn(sender_id);
    await sendMessage(sender_id, response);

    //get facebook username
    let username = await getFacebookUsername(sender_id);

    //send another message
    let response2 = {
      attachment: {
        type: "template",
        payload: {
          template_type: "button",
          text: `Done! \nOur reservation team will contact you as soon as possible ${username}.\n \nWould you like to check our Main Menu?`,
          buttons: [
            {
              type: "postback",
              title: "DỊCH VỤ CHÍNH",
              payload: "MAIN_MENU",
            },
            {
              type: "phone_number",
              title: "☎ HOT LINE",
              payload: "+911911",
            },
            {
              type: "postback",
              title: "START OVER",
              payload: "RESTART_CONVERSATION",
            },
          ],
        },
      },
    };
    await sendTypingOn(sender_id);
    await sendMessage(sender_id, response2);
  } catch (e) {
    console.log(e);
  }
};

let sendNotificationToTelegram = (user) => {
  return new Promise((resolve, reject) => {
    try {
      let request_body = {
        chat_id: process.env.TELEGRAM_GROUP_ID,
        parse_mode: "HTML",
        text: `
| --- <b>A new reservation</b> --- |
| ------------------------------------------------|
| 1. Username: <b>${user.name}</b>   |
| 2. Phone number: <b>${user.phoneNumber}</b> |
| 3. Time: <b>${user.time}</b> |
| 4. Quantity: <b>${user.quantity}</b> |
| 5. Created at: ${user.createdAt} |
| ------------------------------------------------ |                           
      `,
      };

      // Send the HTTP request to the Telegram
      request(
        {
          uri: `https://api.telegram.org/bot${process.env.TELEGRAM_BOT_TOKEN}/sendMessage`,
          method: "POST",
          json: request_body,
        },
        (err, res, body) => {
          if (!err) {
            resolve("done!");
          } else {
            reject("Unable to send message:" + err);
          }
        }
      );
    } catch (e) {
      reject(e);
    }
  });
};

let sendMessageDefaultForTheBot = (sender_psid) => {
  return new Promise(async (resolve, reject) => {
    try {
      let response1 = {
        text: "Xin lỗi! Tôi không thể hiểu được yêu cầu của bạn lúc này ^^ \nHãy giúp đỡ tôi bằng việc nhấn vào những lựa chọn bên dưới nhé.\n\n😉",
      };
      //send a media template
      let response2 = {
        attachment: {
          type: "template",
          payload: {
            template_type: "media",
            elements: [
              {
                media_type: "video",
                url: "https://www.facebook.com/haryphamdev/videos/635394223852656/",
                buttons: [
                  {
                    type: "web_url",
                    url: "https://bit.ly/subscribe-haryphamdev",
                    title: "Watch more!",
                  },
                  {
                    type: "postback",
                    title: "Start over",
                    payload: "RESTART_CONVERSATION",
                  },
                ],
              },
            ],
          },
        },
      };
      await sendTypingOn(sender_psid);
      await sendMessage(sender_psid, response1);
      await sendTypingOn(sender_psid);
      await sendMessage(sender_psid, response2);
      resolve("done");
    } catch (e) {
      reject(e);
    }
  });
};

let showRoomDetail = (sender_psid) => {
  return new Promise(async (resolve, reject) => {
    try {
      let response1 = {
        attachment: {
          type: "image",
          payload: {
            url: URL_SHOW_ROOM_GIF,
          },
        },
      };
      let response2 = {
        attachment: {
          type: "template",
          payload: {
            template_type: "button",
            text: `The rooms is suited for parties up to 45 people.`,
            buttons: [
              {
                type: "postback",
                title: "DỊCH VỤ CHÍNH",
                payload: "MAIN_MENU",
              },
              {
                type: "postback",
                title: "ĐẶT LỊCH KHÁM",
                payload: "RESERVE_TABLE",
              },
            ],
          },
        },
      };

      await sendTypingOn(sender_psid);
      await sendMessage(sender_psid, response1);
      await sendTypingOn(sender_psid);
      await sendMessage(sender_psid, response2);

      resolve("done!");
    } catch (e) {
      reject(e);
    }
  });
};

let sendThanKinh = (sender_psid) => {
  return new Promise(async (resolve, reject) => {
    try {
      let response = {
        attachment: {
          type: "template",
          payload: {
            template_type: "generic",
            elements: [
              {
                title: "PGS-TS Nguyễn Thọ Lộ",
                subtitle: `Nguyên Phó Giám đốc Bệnh viện Quân y 103
                Chuyên gia về Thần kinh Sọ não và Cột sống
                Nguyên Phó chủ tịch Hội Phẫu thuật Thần kinh Việt Nam
                Bác sĩ khám cho người bệnh từ 13 tuổi trở lên
                                Giá khám: 400.000VNĐ`,
                image_url:
                  "https://cdn.bookingcare.vn/fr/w200/2019/12/31/155850-pgs-nguyen-tho-lo.jpg",
              },

              {
                title: "PGS-TS Nguyễn Trọng Hưng",
                subtitle: `Nguyên Trưởng khoa Tâm Thần kinh – Bệnh viện Lão Khoa Trung ương
                Nguyên Bác sỹ Khoa Thần kinh - Bệnh viện Bạch Mai
                Bác sĩ khám cho người bệnh từ 3 tuổi trở lên
                                Giá khám: 500.000VNĐ`,
                image_url:
                  "https://cdn.bookingcare.vn/fr/w200/2020/01/03/084302-pgs-nguyen-trong-hung.jpg",
              },

              {
                title: "TS Nguyễn Văn Doanh",
                subtitle: `Trưởng khoa Khám bệnh, Bệnh viện Đa khoa Quốc tế Thu Cúc
                Nguyên chủ nhiệm khoa thần kinh, Bệnh viện Hữu Nghị Việt Xô
                Bác sĩ có 40 năm kinh nghiệm làm việc chuyên khoa Nội Thần kinh
                Bác sĩ khám cho người bệnh từ 16 tuổi trở lên
                                Giá khám: 150.000VNĐ`,
                image_url:
                  "https://cdn.bookingcare.vn/fr/w200/2017/12/23/170155nguyen-van-doanh.jpg",
              },

              {
                title: "Trở về",
                image_url: " https://digital.fpt.com.vn/wp-content/uploads/2020/07/chamsoctuxa.jpg",
                buttons: [
                  {
                    type: "postback",
                    title: "CHUYÊN KHOA",
                    payload: "BACK_TO_LUNCH_MENU",
                  },
                  {
                    type: "postback",
                    title: "CÁC DỊCH VỤ",
                    payload: "BACK_TO_MAIN_MENU",
                  },
                  {
                    type: "postback",
                    title: "ĐẶT LỊCH KHÁM",
                    payload: "RESERVE_TABLE",
                  },
                ],
              },
            ],
          },
        },
      };

      await sendTypingOn(sender_psid);
      await sendMessage(sender_psid, response);
    } catch (e) {
      reject(e);
    }
  });
};

let sendTieuHoa = (sender_psid) => {
  return new Promise(async (resolve, reject) => {
    try {
      let response = {
        attachment: {
          type: "template",
          payload: {
            template_type: "generic",
            elements: [
              {
                title: "GS-TS Hà Văn Quyết",
                subtitle: `Chuyên gia trên 35 năm kinh nghiệm trong lĩnh vực bệnh lý Tiêu hóa
                Chuyên gia đầu ngành trong lĩnh vực bệnh lý Tiêu hóa
                Nguyên Giám đốc Bệnh viện Đại học Y Hà Nội
                Bác sĩ khám cho người bệnh từ 3 tuổi trở lên`,
                image_url:
                  "https://cdn.bookingcare.vn/fr/w200/2019/12/31/155650-gs-ha-van-quyet.jpg",
              },

              {
                title: "BS Tiết Kim Phong",
                subtitle: `Bác sĩ đang công tác tại Phòng khám Đa khoa Quốc tế Nhân Hậu`,
                image_url:
                  "https://cdn.bookingcare.vn/fr/w200/2020/08/11/152331-bs-tiet-kim-phong-2.jpg",
              },

              {
                title: "GS-TS Nguyễn Cường Thịnh",
                subtitle: `Chủ nhiệm Bộ môn Phẫu thuật Tiêu hóa, Bệnh viện Trung ương Quân đội 108
                Hơn 35 kinh nghiệm trong lĩnh vực Tiêu hóa, Gan, Mật, Tụỵ, Hậu môn, Trực tràng
                Bác sĩ nhận bệnh nhân từ 18 tuổi trở lên`,
                image_url:
                  "https://cdn.bookingcare.vn/fr/w200/2020/06/23/160330-bs-cuong-thinh.jpg",
              },

              {
                title: "Trở về",
                image_url: " https://digital.fpt.com.vn/wp-content/uploads/2020/07/chamsoctuxa.jpg",
                buttons: [
                  {
                    type: "postback",
                    title: "CHUYÊN KHOA",
                    payload: "BACK_TO_LUNCH_MENU",
                  },
                  {
                    type: "postback",
                    title: "CÁC DỊCH VỤ",
                    payload: "BACK_TO_MAIN_MENU",
                  },
                  {
                    type: "postback",
                    title: "ĐẶT LỊCH KHÁM",
                    payload: "RESERVE_TABLE",
                  },
                ],
              },
            ],
          },
        },
      };

      await sendTypingOn(sender_psid);
      await sendMessage(sender_psid, response);
    } catch (e) {
      reject(e);
    }
  });
};

let sendTim = (sender_psid) => {
  return new Promise(async (resolve, reject) => {
    try {
      let response = {
        attachment: {
          type: "template",
          payload: {
            template_type: "generic",
            elements: [
              {
                title: "GS-TS Nguyễn Văn Quýnh",
                subtitle: `Nguyên Phó Chủ nhiệm Bộ môn Nội tim mạch, Bệnh viện Trung ương Quân đội 108
                Chuyên gia hàng đầu về nội tim mạch với hơn 30 năm kinh nghiệm
                Bác sĩ khám cho người bệnh từ 18 tuổi trở lên`,
                image_url:
                  "https://cdn.bookingcare.vn/fr/w200/2022/05/05/104945-nguyen-van-quynh-pgs.jpg",
              },

              {
                title: "GS-TS Nguyễn Lân Việt",
                subtitle: `Nguyên Viện trưởng Viện Tim Mạch Quốc Gia
                Nguyên Hiệu trưởng trường Đại học Y Hà Nội
                Chủ tịch Hội Tim mạch Việt Nam`,
                image_url:
                  "https://cdn.bookingcare.vn/fr/w200/2016/08/29/120727gs-ts-nguyen-lan-viet.jpg",
              },

              {
                title: "BS Hứa Thị Ngọc Hà",
                subtitle: `Nguyên Trưởng khoa Nội tim mạch, Bệnh viện 198 - Bộ Công An
                Hiện là Trưởng khoa Khám bệnh, Bệnh viện Đông Đô`,
                image_url:
                  "https://cdn.bookingcare.vn/fr/w200/2020/01/03/085845-bscki-hua-thi-ngoc-ha.jpg",
              },

              {
                title: "Trở về",
                image_url: " https://digital.fpt.com.vn/wp-content/uploads/2020/07/chamsoctuxa.jpg",
                buttons: [
                  {
                    type: "postback",
                    title: "CHUYÊN KHOA",
                    payload: "BACK_TO_LUNCH_MENU",
                  },
                  {
                    type: "postback",
                    title: "CÁC DỊCH VỤ",
                    payload: "BACK_TO_MAIN_MENU",
                  },
                  {
                    type: "postback",
                    title: "ĐẶT LỊCH KHÁM",
                    payload: "RESERVE_TABLE",
                  },
                ],
              },
            ],
          },
        },
      };

      await sendTypingOn(sender_psid);
      await sendMessage(sender_psid, response);
    } catch (e) {
      reject(e);
    }
  });
};

let sendTaiMuiHong = (sender_psid) => {
  return new Promise(async (resolve, reject) => {
    try {
      let response = {
        attachment: {
          type: "template",
          payload: {
            template_type: "generic",
            elements: [
              {
                title: "PGS-TS Nguyễn Thị Hoài An",
                subtitle: `Nguyên Trưởng khoa Tai mũi họng trẻ em, Bệnh viện Tai Mũi Họng Trung ương
                Trên 25 năm công tác tại Bệnh viện Tai mũi họng Trung ương
                Chuyên khám và điều trị các bệnh lý Tai Mũi Họng người lớn và trẻ em`,
                image_url:
                  "https://cdn.bookingcare.vn/fr/w200/2020/01/03/090559-pgs-nguyen-thi-hoai-an.jpg",
              },

              {
                title: "GS-TS Nguyễn Văn Lý",
                subtitle: `Nguyên Trưởng khoa Tai mũi họng, Bệnh viện Trung ương quân đội 108
                Ủy viên Ban chấp hành Hội Tai Mũi Họng Việt Nam
                Bác sĩ khám từ 14 tuổi trở lên`,
                image_url:
                  "https://cdn.bookingcare.vn/fr/w200/2021/06/26/121515-bs-nguyen-van-ly.jpg",
              },

              {
                title: "BS Nguyễn Ngọc Phấn",
                subtitle: `Nguyên bác sĩ Bệnh viện Đa khoa Hồng Ngọc
                Hiện đang công tác tại Bệnh viện Đại học Y Hà Nội`,
                image_url:
                  "https://cdn.bookingcare.vn/fr/w200/2019/06/25/165749bs-nguyen-ngoc-phan.jpg",
              },

              {
                title: "Trở về",
                image_url: " https://digital.fpt.com.vn/wp-content/uploads/2020/07/chamsoctuxa.jpg",
                buttons: [
                  {
                    type: "postback",
                    title: "CHUYÊN KHOA",
                    payload: "BACK_TO_LUNCH_MENU",
                  },
                  {
                    type: "postback",
                    title: "CÁC DỊCH VỤ",
                    payload: "BACK_TO_MAIN_MENU",
                  },
                  {
                    type: "postback",
                    title: "ĐẶT LỊCH KHÁM",
                    payload: "RESERVE_TABLE",
                  },
                ],
              },
            ],
          },
        },
      };

      await sendTypingOn(sender_psid);
      await sendMessage(sender_psid, response);
    } catch (e) {
      reject(e);
    }
  });
};

let sendMessage = (sender_psid, response) => {
  return new Promise((resolve, reject) => {
    try {
      let request_body = {
        recipient: {
          id: sender_psid,
        },
        message: response,
      };

      // Send the HTTP request to the Messenger Platform
      request(
        {
          uri: "https://graph.facebook.com/v6.0/me/messages",
          qs: { access_token: PAGE_ACCESS_TOKEN },
          method: "POST",
          json: request_body,
        },
        (err, res, body) => {
          console.log(res);
          console.log(body);
          if (!err) {
            console.log("message sent!");
            resolve("done!");
          } else {
            reject("Unable to send message:" + err);
          }
        }
      );
    } catch (e) {
      reject(e);
    }
  });
};

let sendTypingOn = (sender_psid) => {
  return new Promise((resolve, reject) => {
    try {
      let request_body = {
        recipient: {
          id: sender_psid,
        },
        sender_action: "typing_on",
      };

      // Send the HTTP request to the Messenger Platform
      request(
        {
          uri: "https://graph.facebook.com/v6.0/me/messages",
          qs: { access_token: PAGE_ACCESS_TOKEN },
          method: "POST",
          json: request_body,
        },
        (err, res, body) => {
          if (!err) {
            resolve("done!");
          } else {
            reject("Unable to send message:" + err);
          }
        }
      );
    } catch (e) {
      reject(e);
    }
  });
};

let markMessageSeen = (sender_psid) => {
  return new Promise((resolve, reject) => {
    try {
      let request_body = {
        recipient: {
          id: sender_psid,
        },
        sender_action: "mark_seen",
      };

      // Send the HTTP request to the Messenger Platform
      request(
        {
          uri: "https://graph.facebook.com/v6.0/me/messages",
          qs: { access_token: PAGE_ACCESS_TOKEN },
          method: "POST",
          json: request_body,
        },
        (err, res, body) => {
          if (!err) {
            resolve("done!");
          } else {
            reject("Unable to send message:" + err);
          }
        }
      );
    } catch (e) {
      reject(e);
    }
  });
};

module.exports = {
  getFacebookUsername: getFacebookUsername,
  sendResponseWelcomeNewCustomer: sendResponseWelcomeNewCustomer,
  sendMainMenu: sendMainMenu,
  sendLunchMenu: sendLunchMenu,
  sendDinnerMenu: sendDinnerMenu,
  sendPubMenu: sendPubMenu,
  sendAppetizer: sendAppetizer,
  goBackToMainMenu: goBackToMainMenu,
  goBackToLunchMenu: goBackToLunchMenu,
  handleReserveTable: handleReserveTable,
  handleShowRooms: handleShowRooms,
  sendMessageAskingQuality: sendMessageAskingQuality,
  sendMessageAskingPhoneNumber: sendMessageAskingPhoneNumber,
  sendMessageDoneReserveTable: sendMessageDoneReserveTable,
  sendNotificationToTelegram: sendNotificationToTelegram,
  sendMessageDefaultForTheBot: sendMessageDefaultForTheBot,
  showRoomDetail: showRoomDetail,
  sendThanKinh: sendThanKinh,
  sendTieuHoa: sendTieuHoa,
  sendTaiMuiHong: sendTaiMuiHong,
  sendTim: sendTim,
  markMessageSeen: markMessageSeen,
  sendTypingOn: sendTypingOn,
  sendMessage: sendMessage,
};
